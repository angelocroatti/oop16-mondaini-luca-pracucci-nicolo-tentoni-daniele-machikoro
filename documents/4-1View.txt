Autovalutazione: Non � stato un lavoro facile, ho impiegato molto tempo nel gestire le componenti grafiche e adattarle
ad ogni possibile schermo.
Oltre a questo anche la gestione di panel innestati con molti componenti all'interno mi ha portato via del tempo.
E' stato il mio primo programma creato in gruppo e inizialmente non � stato facile coordinarsi con gli altri.
Tutto sommato per� penso sia venuto fuori un bel progetto, sicuramente ha bisogno di qualche miglioramento, ma si
parla di aggiornamenti futuri.
Il tempo a disposizione non � stato tale da riuscire a creare un'applicazione che utilizzasse la rete locale LAN,
evitando di giocare tutti sullo stesso dispositivo.
Io e i membri del progetto siamo intenzionati in un futuro ad implementare questa funzione.